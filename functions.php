<?php

// Adicionar arquivos .css e .js

function add_styles_and_scripts() {
    
	// all styles
	wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');

	// all scripts
	wp_enqueue_script( 'jquery-3.4.1', get_template_directory_uri() . '/assets/js/jquery-3.4.1.js');
	wp_enqueue_script( 'menu-hamburguer', get_template_directory_uri() . '/assets/js/menu-hamburguer.js', array( 'jquery-3.4.1' ));
    
}
add_action( 'wp_enqueue_scripts', 'add_styles_and_scripts' );

// Post Type Blog

function custom_post_type_noticia() {
	register_post_type('noticia', array(
		'label' => 'Noticia',
        'description' => 'Noticia',
        'menu_position' =>  4,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'query_var' => true,
		'supports' => array('title', 'editor'),

		'labels' => array (
			'name' => 'Noticia',
			'singular_name' => 'Noticia',
			'menu_name' => 'Noticia',
			'add_new' => 'Noticia Nova',
			'add_new_item' => 'Adicionar Nova Noticia',
			'edit' => 'Editar',
			'edit_item' => 'Editar Noticia',
			'new_item' => 'Nova Noticia',
			'view' => 'Ver Noticia',
			'view_item' => 'Ver Noticia',
			'search_items' => 'Procurar Noticia',
			'not_found' => 'Nenhuma Noticia Encontrada',
			'not_found_in_trash' => 'Nenhuma Noticia Encontrada no Lixo',
		)
	));
}
add_action('init', 'custom_post_type_noticia');

?>