<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <title>DGR-Contadora</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Aldrich|PT+Serif&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"> -->
 <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css"> -->
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head(); ?>
</head>

<body>
    <header>
    <?php $contato = get_page_by_title('Home');?>
        <section class="container-plus navbar navbar-expand-lg navbar-expand-md ">
            <div class="logo">
                <a href="Home"><img src="<?php echo get_template_directory_uri();?>/assets/img/DGR.png" class="img-header" alt="DGR"></a>
            </div>
            <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="menu collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="menu-list navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="servicos" class="nav-link">Serviços</a>
                    </li>
                    <li class="nav-item">
                        <a href="noticias" class="nav-link">Notícias</a>
                    </li>
                    <li class="nav-item">
                        <a href="Contato" class="nav-link">Contato</a>
                    </li>
                    <li class="nav-item">
                        <div class="menu-tel">
                            <p id="first-tel"><?php the_field('telefone1', $contato); ?></p>
                            <p id="second-tel"><?php the_field('telefone2', $contato); ?></p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    </header>