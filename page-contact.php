<?php
	//Template Name: Contato
?>

<?php get_header(); ?>
<section>
    <form action="" class="form-contact container">
        <h1>Fale Conosco</h1>
        <div class="input-div">
            <label for="name"><b>Nome:</b></label>
            <input type="text" placeholder="Ex.: João da Silva" name="name">
        </div>
        <div class="input-div">
            <label for="email"><b>E-mail:</b></label>
            <input type="text" placeholder="Ex.: joao-silva@exemplo.com" name="email">
        </div>
        <div class="input-div">
            <label for="subject"><b>Assunto:</b></label>
            <input type="text" placeholder="Ex.: Abertura de empresa" name="subject">
        </div>
        <div class="input-div">
            <label for="textarea"><b>Mensagem:</b></label>
            <textarea placeholder="Digite aqui sua mensagem..." name="Área de Texto"></textarea>
        </div>
        <div class = "button-area">
            <button type="submit" class="registerbtn clear">Enviar</button>
        </div>
    </form>
</section>
<?php get_footer(); ?>