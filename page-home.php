<?php
	//Template Name: Home
?>

<?php get_header(); ?>

<section class="first-section">
	<!-- <img src="<?php echo get_template_directory_uri();?>/assets/img/background-home.png"> -->
	<div class="center-itens container">
		<img src="<?php echo get_template_directory_uri();?>/assets/img/balance.png">
		<h2>Soluções contábeis e jurídicas</h2>
		<a href="Contato" class="bt-contato">Entre em contato</a>
	</div>
</section>
<section class="second-section">
	<div class="values container">
		<div class="values-item">
			<h2>Missão</h2>
			<p><?php the_field('missao_descricao'); ?></p>
		</div>
		<div class="values-item">
			<h2>Visão</h2>
			<p><?php the_field('visao_descricao'); ?></p>
		</div>
		<div class="values-item">
			<h2>Negócio</h2>
			<p><?php the_field('negocio_descricao'); ?></p>
		</div>
	</div>
	<div class="texto container">
	<p><?php the_field('texto_descricao'); ?></p>
	</div>
</section>
<?php get_footer(); ?>