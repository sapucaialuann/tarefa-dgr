<?php
	//Template Name: Notícias
?>

<?php get_header(); ?>

<section class="our-blog">
    <div class="container">
        <div class="recent-posts">
            <div class="title">
                <!-- <h2>Nosso blog</h2> -->
                <h3>Notícias recentes</h3>
                <h4>Notícias</h4>
            </div>
            <div class="description">
                <div class="posts">
                    <?php
                    $noticia = new WP_Query(
                        array(
                            'posts_per_page'   => 3,
                            'post_type'        => 'noticia',
                            'post_status'      => 'publish',
                            'suppress_filters' => true,
                            'orderby'          => 'post_date',
                            'order'            => 'DESC',
                            'paged'            => max(1, get_query_var('paged'))
                        )
                    );
                    ?>
                    <?php if ($noticia->have_posts()) :
                        while ($noticia->have_posts()) : $noticia->the_post(); ?>

                            <div class="posts-item wow fadeInUp">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="posts-item-wrapper">
                                        <div class="posts-item-img">
                                            <img src="<?php the_field('imagem_noticia'); ?>" alt="<?php the_field('titulo_noticia'); ?>">
                                        </div>
                                    </div>
                                    <h4><?php the_field('titulo_noticia'); ?></h4>
                                    <p class="post-date"><?php the_field('hora_noticia'); ?></p>
                                    <p><?php the_field('subtitulo_noticia'); ?></p>
                                </a>
                            </div>

                        <?php endwhile; ?>

                        <div class="paginate">
                            <?php
                            //chamar a paginacao
                            $total_pages = $noticia->max_num_pages;

                            if ($total_pages > 1) {
                                $current_page = max(1, get_query_var('paged'));

                                echo paginate_links(array(
                                    'base'         => get_pagenum_link(1) . '%_%',
                                    'format'     => '/page/%#%',
                                    'current'     => $current_page,
                                    'total'     => $total_pages,
                                    'prev_next'     => true,
                                    'prev_text'     => __('<'),
                                    'next_text'     => __('>')
                                ));
                            }
                            ?>
                        </div>

                    <?php else : ?>
                        <p>Nenhuma notícia disponível</p>
                    <?php endif; ?>
                    <?php wp_reset_postdata(); ?>

                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>