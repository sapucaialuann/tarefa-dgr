<?php
	//Template Name: Serviços
?>

<?php get_header (); ?>

    <section class="our-services">
        <div class="container">
            <div class="page-intro">
                <h1>Nossos Serviços</h1>
                <p><?php the_field('texto_introducao'); ?></p>
            </div>
            <div class="card-services">
                <div class="card-item card-left">
                    <h3><?php the_field('titulo_card_1'); ?></h3>
                    <ul>
                        <li>&#9679; <?php the_field('assunto_1_card_1'); ?></li>
                        <li>&#9679; <?php the_field('assunto_2_card_1'); ?></li>
                        <li>&#9679; <?php the_field('assunto_3_card_1'); ?></li>
                        <li>&#9679; <?php the_field('assunto_4_card_1'); ?></li>
                        <li>&#9679; <?php the_field('assunto_5_card_1'); ?></li>
                        <li>&#9679; <?php the_field('assunto_6_card_1'); ?></li>
                        <li>&#9679; <?php the_field('assunto_7_card_1'); ?></li>
                    </ul>
                </div>
                <div class="card-item card-center">
                    <h3><?php the_field('titulo_card_2'); ?></h3>
                    <ul>
                        <li>&#9679; <?php the_field('assunto_1_card_2'); ?></li>
                        <li>&#9679; <?php the_field('assunto_2_card_2'); ?></li>
                        <li>&#9679; <?php the_field('assunto_3_card_2'); ?></li>
                        <li>&#9679; <?php the_field('assunto_4_card_2'); ?></li>
                        <li>&#9679; <?php the_field('assunto_5_card_2'); ?></li>
                        <li>&#9679; <?php the_field('assunto_6_card_2'); ?></li>
                        <li>&#9679; <?php the_field('assunto_7_card_2'); ?></li>
                    </ul>
                </div>
                <div class="card-item card-right">
                    <h3><?php the_field('titulo_card_3'); ?></h3>
                    <ul>
                        <li>&#9679; <?php the_field('assunto_1_card_3'); ?></li>
                        <li>&#9679; <?php the_field('assunto_2_card_3'); ?></li>
                        <li>&#9679; <?php the_field('assunto_3_card_3'); ?></li>
                        <li>&#9679; <?php the_field('assunto_4_card_3'); ?></li>
                        <li>&#9679; <?php the_field('assunto_5_card_3'); ?></li>
                        <li>&#9679; <?php the_field('assunto_6_card_3'); ?></li>
                        <li>&#9679; <?php the_field('assunto_7_card_3'); ?></li>
                    </ul>
                </div>
            </div>
        </div>  
    </section>

<?php get_footer (); ?>