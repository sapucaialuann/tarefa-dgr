<? php // Template Name: Single-Noticia
?>

<?php get_header(); ?>

<section id="notice">
    <div class="container">
        <div class="notice-content">
            <div class="notice-header">
                <div class="notice-title">
                    <h1><?php the_field('titulo_noticia'); ?></h1>
                    <h2><?php the_field('subtitulo_noticia'); ?></h2>
                </div>
                <div class="notice-subtitle">
                    <p class="notice-author">Por <?php the_field('autor_noticia'); ?></p>
                    <p class="notice-time">Publicado em: <?php the_field('hora_noticia'); ?></p>
                </div>
            </div>
            <div class="notice-scope">
                <p><?php the_field('corpo_noticia'); ?></p>
                <img src="<?php the_field('imagem_noticia'); ?>" alt="">
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>